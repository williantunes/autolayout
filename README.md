# Autolayout

This was a challenge developed during the Apple Developer Academy Foundations course. The idea was to adapt the app using Autolayout for portrait/landscape and iPhone 8/iPhone 8 Plus according to the design instructions provided.